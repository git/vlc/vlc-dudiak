#include <CoreFoundation/CoreFoundation.h>
#include <CoreServices/CoreServices.h>
#include <QuickLook/QuickLook.h>
#import <Cocoa/Cocoa.h>

/* -----------------------------------------------------------------------------
 Generate a preview for file
 
 This function's job is to create preview for designated file
 ----------------------------------------------------------------------------- */

OSStatus GeneratePreviewForURL(void *thisInterface, QLPreviewRequestRef preview, CFURLRef url, CFStringRef contentTypeUTI, CFDictionaryRef options)
{
	NSAutoreleasePool *pool;
    NSMutableDictionary *props,*imgProps;
    //NSManagedObject *occasion=NULL;
    NSMutableString *html;
    //NSString *momPath;
    NSData *media;
	NSError *failure;
	
	
    pool = [[NSAutoreleasePool alloc] init];
	
	props = [[[NSMutableDictionary alloc] init] autorelease];
	[props setObject:@"UTF-8" forKey:(NSString *)kQLPreviewPropertyTextEncodingNameKey];
	[props setObject:@"text/html" forKey:(NSString *)kQLPreviewPropertyMIMETypeKey];
	
	//media = [[NSData alloc] initWithContentsOfFile:[(NSURL *)url path]];
	media = [NSData dataWithContentsOfFile:@"/Users/edudiak/Desktop/Green Greens.flac" options:NSUncachedRead error:(&failure)];
	imgProps=[[[NSMutableDictionary alloc] init] autorelease];
//	[imgProps setObject:@"UTF-16" forKey:(NSString *)kQLPreviewPropertyTextEncodingNameKey];
	[imgProps setObject:@"application/x-vlc-plugin" forKey:(NSString *)kQLPreviewPropertyMIMETypeKey];
	[imgProps setObject:media forKey:(NSString *)kQLPreviewPropertyAttachmentDataKey];
	[props setObject:[NSDictionary dictionaryWithObject:imgProps forKey:@"media.vlc"] forKey:(NSString *)kQLPreviewPropertyAttachmentsKey];
	
	[props setObject:@"RFC 2156" forKey:(NSString *)kQLPreviewContentIDScheme];
	
    html=[[[NSMutableString alloc] init] autorelease];
    [html appendString:@"<html><body bgcolor=white>"];
	//[html appendString:@"<object classid=\"application/x-shockwave-flash\" width=\"1000\" height=\"400\" src=\"cid:media.vlc\" /><PARAM NAME=movie VALUE=\"cid:media.vlc\"></object><br>"];
	[html appendString:@"<object src=\"cid:media.vlc\" width=\"300\" height=\"120\" type=\"application/x-vlc-plugin\"></object><br>"];
    [html appendString:@"<br>powered by VLC<br>"];
	[html appendString:[failure domain]];
    [html appendString:@"</body></html>"];
	//[html appendString:CFURLGetString(url)];
	
	//system("open /Applications/VLC.app");
	//[[(NSURL *)url] path];
	//image=(NSData *)CFURLCreateData(kCFAllocatorDefault, url, kCFStringEncodingMacHFS, FALSE);
	
	
    QLPreviewRequestSetDataRepresentation(
                                          preview,
                                          (CFDataRef)[html dataUsingEncoding:NSUTF8StringEncoding],
                                          kUTTypeHTML,
                                          (CFDictionaryRef)props);
    
    [pool release];
    return noErr;
}

void CancelPreviewGeneration(void* thisInterface, QLPreviewRequestRef preview)
{
    // implement only if supported
}
