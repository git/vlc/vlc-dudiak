#include <CoreFoundation/CoreFoundation.h>
#include <CoreServices/CoreServices.h>
#include <QuickLook/QuickLook.h>
#import <Cocoa/Cocoa.h>

/* -----------------------------------------------------------------------------
 Generate a preview for file
 
 This function's job is to create preview for designated file
 ----------------------------------------------------------------------------- */

OSStatus GeneratePreviewForURL(void *thisInterface, QLPreviewRequestRef preview, CFURLRef url, CFStringRef contentTypeUTI, CFDictionaryRef options)
{
	NSAutoreleasePool *pool;
	NSMutableDictionary *props;
	NSString *inPath = [(NSURL *)url path];
	NSMutableString *command;
	NSString *outPath = [NSTemporaryDirectory() stringByAppendingPathComponent:@".vlc_quicklook.mov"];
    NSData *media;
   	
	pool = [[NSAutoreleasePool alloc] init];
    
    //Check for Perian installation to remove VLC_QuickLook
    //NOTE: This assumes Perian is named Perian.component and will need to be updated if that is no longer true
	if([[NSFileManager defaultManager] fileExistsAtPath:[[NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSLocalDomainMask, YES)
                                                          objectAtIndex:0] stringByAppendingPathComponent:@"/QuickTime/Perian.component"]] ||
       [[NSFileManager defaultManager] fileExistsAtPath:[[NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES)
                                                          objectAtIndex:0] stringByAppendingPathComponent:@"/QuickTime/Perian.component"]]) {
        props = [[[NSMutableDictionary alloc] init] autorelease];
        [props setObject:[NSNumber numberWithInt:640] forKey:(NSString *)kQLPreviewPropertyWidthKey];
        [props setObject:[NSNumber numberWithInt:480] forKey:(NSString *)kQLPreviewPropertyHeightKey];
        [props setObject:@"Full Preview (with Perian)" forKey:(NSString *)kQLPreviewPropertyDisplayNameKey];
        
        media = [NSData dataWithContentsOfFile:inPath];
        
        //Start the preview
        QLPreviewRequestSetDataRepresentation(preview,
                                              (CFDataRef)media,
                                              kUTTypeMovie,
                                              (CFDictionaryRef)props);
        
    } //if Perian is not installed, VLC should make a 15 second preview
    else {
        //Set up properties for the quick look preview
        props = [[[NSMutableDictionary alloc] init] autorelease];
        [props setObject:[NSNumber numberWithInt:640] forKey:(NSString *)kQLPreviewPropertyWidthKey];
        [props setObject:[NSNumber numberWithInt:480] forKey:(NSString *)kQLPreviewPropertyHeightKey];
        [props setObject:@"15 Second VLC Preview (double-click video to play in VLC)" forKey:(NSString *)kQLPreviewPropertyDisplayNameKey];
        
        //Create shell command to lauch VLC and provide correct arguments to VLC for transcoding
        command = [[[NSMutableString alloc] init] autorelease];
        [command appendString:[[NSSearchPathForDirectoriesInDomains(NSApplicationDirectory, NSSystemDomainMask, YES) 
                                objectAtIndex:0] stringByAppendingPathComponent:@"/VLC.app/Contents/MacOS/clivlc"]];
        [command appendString:@" -I dummy \""];
        [command appendString:inPath];
        [command appendString:@"\" :sout=\"#transcode{vcodec=mp4v,vb=1024,scale=1,acodec=mp4a,ab=128,channels=2}:std{access=file,mux=mov,dst="];
        [command appendString:outPath];
        [command appendString:@"}\" --stop-time=15 vlc:quit"];
        
        //Launch VLC to transcode VLC-compatible file into QT-compatible file
        system([command UTF8String]);
        
        //Load the newly converted file
        media = [NSData dataWithContentsOfFile:outPath];
        
        //Start the preview
        QLPreviewRequestSetDataRepresentation(preview,
                                              (CFDataRef)media,
                                              kUTTypeQuickTimeMovie,
                                              (CFDictionaryRef)props);
    }
    
    [pool release];
    return noErr;
}

void CancelPreviewGeneration(void* thisInterface, QLPreviewRequestRef preview)
{
    // implement only if supported
}
